import { Form, Input, InputNumber, Modal, Select } from "antd";
import { useEffect } from "react";

const { Option } = Select;

const EditProduct = ({ showModalEdit, setShowModalEdit, updateProducts }) => {
  const [form] = Form.useForm();

  useEffect(() => {
    if (showModalEdit) {
      form.setFieldsValue({
        ...showModalEdit,
      });
    }
  }, [showModalEdit]);


  return (
    <>

      <Modal
        title="Chỉnh sửa banner"
        visible={showModalEdit}
        okText="Cập nhật"
        cancelText="Hủy"
        onOk={() => form.submit()}
        onCancel={() => setShowModalEdit(false)}
      >
        <Form form={form} onFinish={updateProducts}
          layout="vertical" className=" mx-auto">
          <Form.Item
            label="Tên sản phẩm"
            name="name"
            rules={[
              {
                required: true,
                message: 'Tên sản phẩm không được để trống!',
              },
            ]}
          >
            <Input />
          </Form.Item>

          <Form.Item
            label="Giá gốc"
            name="price"
            rules={[
              {
                required: true,
                message: 'Giá không được để trống!',
              },
            ]}
          >
            <InputNumber className="w-full" />
          </Form.Item>
          <Form.Item
            label="Phần trăm giảm giá"
            name="percent_sale"

          >
            <InputNumber className="w-full" />
          </Form.Item>
          <Form.Item
            label="Số lượng"
            name="amount"
            rules={[
              {
                required: true,
                message: 'Số lượng không được để trống!',
              },
            ]}
          >
            <InputNumber className="w-full" />

          </Form.Item>
          <Form.Item
            label="Hình sản phâm"
            name="image"
            rules={[
              {
                required: true,
                message: 'Hình sản phẩm không được để trống!',
              },
            ]}
          >
            <Input />

          </Form.Item>

        </Form>
      </Modal>
    </>
  );
};

export default EditProduct;
