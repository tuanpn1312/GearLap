import { Button, Form, Input, InputNumber, Modal, Select } from "antd";
import { useState } from "react";

export default function AddProduct({ brands, categories, deleteImage, handleChangeImage, onFinishFailed, isModalAddProduct, handleCancelAddProduct, handleaddProduct }) {

    const [selectedType, setSelectedType] = useState('');

    return (
        <Modal title="Thêm sản phẩm" visible={isModalAddProduct} onCancel={handleCancelAddProduct} footer={null}

        >
            <div className="flex flex-col space-y-4">

                <Form
                    layout="vertical"
                    name="basic"
                    onFinish={handleaddProduct}
                    onFinishFailed={onFinishFailed}
                    autoComplete="off"
                >
                    <Form.Item
                        label="Tên sản phẩm"
                        name="name"
                        rules={[
                            {
                                required: true,
                                message: 'Tên sản phẩm không được để trống!',
                            },
                        ]}
                    >
                        <Input />
                    </Form.Item>
                    <Form.Item
                        label="Loại sản phẩm"
                        name="type_id"
                        rules={[
                            {
                                required: true,
                                message: 'Loại sản phẩm không được để trống!',
                            },
                        ]}
                    >
                        <Select onChange={(value) => setSelectedType(value)}>
                            {categories.map((item) => (
                                <Option key={item.id}
                                    value={item.id}>
                                    {item.name}
                                </Option>
                            ))}
                        </Select>

                    </Form.Item>
                    <Form.Item
                        label="Hãng"
                        name="brand_id"
                        rules={[
                            {
                                required: true,
                                message: 'Hãng không được để trống!',
                            },
                        ]}
                    >
                        <Select disabled={!selectedType}>
                            {selectedType && categories.find(item => item.id === selectedType).brand.map((item) => (
                                <Option key={item.brand_id} value={item.brand_id}>
                                    {item.name}
                                </Option>
                            ))}
                        </Select>

                    </Form.Item>
                    <Form.Item
                        label="Giá gốc"
                        name="price"
                        rules={[
                            {
                                required: true,
                                message: 'Giá không được để trống!',
                            },
                        ]}
                    >
                        <InputNumber className="w-full" />
                    </Form.Item>
                    <Form.Item
                        label="Phần trăm giảm giá"
                        name="percent_sale"
                        rules={[
                            {
                                required: true,
                                message: 'Giá không được để trống!',
                            },
                        ]}
                    >
                        <InputNumber className="w-full" />
                    </Form.Item>
                    <Form.Item
                        label="Số lượng"
                        name="amount"
                        rules={[
                            {
                                required: true,
                                message: 'Số lượng không được để trống!',
                            },
                        ]}
                    >
                        <InputNumber className="w-full" />

                    </Form.Item>
                    <Form.Item
                        label="Thời gian bảo hành"
                        name="warranty"
                        rules={[
                            {
                                required: true,
                                message: 'Bảo hành không được để trống!',
                            },
                        ]}
                    >
                        <InputNumber className="w-full" />

                    </Form.Item>
                    <Form.Item
                        label="Ảnh sản phẩm"
                        name="image"
                        rules={[
                            {
                                required: true,
                                message: 'Ảnh sản phẩm không được để trống!',
                            },
                        ]}
                    >
                        <Input />

                    </Form.Item>
                    <Form.Item
                        label="Link mô tả sản phẩm"
                        name="link"
                        rules={[
                            {
                                required: true,
                                message: 'Link mô tả sản phẩm không được để trống!',
                            },
                        ]}
                    >
                        <Input />

                    </Form.Item>
                    <Form.Item
                        className="text-center"
                    >
                        <Button htmlType="submit" style={{ width: "100%" }} >Thêm sản phẩm mới</Button>
                    </Form.Item>

                </Form>
            </div>
        </Modal>
    )
}